��    P      �  k         �     �     �  
   �     �  
   �     �  	   �  
          	        $     ,  
   0  
   ;     F     T  
   b     m     t  
   �     �     �  
   �     �  
   �     �     �     �     �     �     �     �                         '     ,     2     9     >     J     W     d     k  	   t  7   ~     �     �     �     �     �  	   �     �     	     7	     P	     k	     �	  !   �	     �	     �	     �	     �	     �	     �	     
  $   5
     Z
     z
     �
     �
     �
     �
  !     '   &  ,   N     {  %   �  �  �     �     �     �     �     �     �  
   �     �     �  
   �  
                  %     4     B  
   Q     \     b     w     �     �     �     �     �     �     �     �     �     �     �     �                    !     '     -     5     <     A     N     ^     p     y  	   �  9   �     �  	   �     �     �                    1     O     f     |     �     �     �     �     �     �     �            #   8     \     z     �     �     �     �      �  &     )   @     j  #   �            3   :             /   C      -      ;      .               	   )       1              5              9   =       I   J       H         N   7             <   (             4   D   K   G                P               >                   ,      !   0                      B      E   
      #   @   8   *       &   "   '   +   L                 M   ?   O   %   $       F      A   6   2        1 day 1 hour 10 minutes 12 hours 15 minutes 2 hours 2 minutes 30 minutes 5 hours 5 minutes Actions Add Add Domain Add Record Are you sure? Automatic TTL Change DNS Choose Confirm Password Current NS DNS Danish Delete DNS Domain Domain DNS Domain Name Domains Edit Edit Profile Email English Enter your old password Expiring Home Information Language List Login Logout Name Nameservers New Password Old Password Our NS Password Permanent Please, tell us your email, otherwise you cannot login! Please, tell us your name! Priority Profile Redirect Type TTL Temporary The domain are now added! The domain does already exist! The record doesn't exist The record is now deleted! The record is now updated! The value isn\'t a valid IP! This type of file is not allowed! Type URL Forward Update Value You have to enter in a TTL You have to enter in a URL You have to enter in a name You have to enter in a new password! You have to enter in a priority You have to enter in a type You have to enter in a value You have to upload something! Your email isn\'t valid! Your file hits the limit! Your new password doesn\'t match! Your old password isn\'t the right one! Your password has to be 6 or more characters Your profile is now updated! Your profile password is now updated! Project-Id-Version: 
POT-Creation-Date: 2018-01-05 13:04+0100
PO-Revision-Date: 2018-01-05 13:04+0100
Last-Translator: 
Language-Team: 
Language: da_DK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: Cache
X-Poedit-SearchPathExcluded-1: node_modules
X-Poedit-SearchPathExcluded-2: public
 1 dag 1 time 10 minutter 12 timer 15 minutter 2 timer 2 minutter 30 minutter 5 timer 5 minutter Handlinger Tilføj Tilføj Domæne Tilføj Record Er du sikker? Automatisk TTL Ændre DNS Vælg Bekræft Adgangskode Nuværende NS DNS Dansk Slet DNS Domæne Domæne DNS Domænenavn Domæner Rediger Rediger profil E-mail Engelsk Indtast dit gamle kodeord Udløbsdato Forside Information Sprog Liste Log ind Log ud Navn Navneservere Nye Adgangskode Gamle Adgangskode Vores NS Adgangskode Permanent Indtast venligst din email, ellers land u ikke logge ind! Indtast venligst dit navn! Prioritet Profil Omdirigeringstype TTL Midlertidig Domænet er nu tilføjet! Domænet eksisterer allerede! Record eksisterer ikke Record er nu slettet! Record er nu opdateret! Angiv en gyldig IP-adresse! Denne filtype er ikke tilladt! Type URL Forward Opdater Værdi Du skal indtaste en TTL Du skal indtaste en URL Du skal indtaste et navn Du skal indtaste en ny adgangskode! Du skal indtaste en prioritet Du skal indtaste en type Du skal indtaste en værdi Du skal uploade noget! Din email er ikke gyldig! Din fil rammer grænsen! Din nye adgangskode passer ikke! Din gamle kodeord er ikke den rigtige! Dit kodeord skal være 6 eller flere tegn Din profil er opdateret Dit profil kodeord er nu opdateret! 