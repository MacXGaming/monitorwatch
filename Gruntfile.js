'use strict';

module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-php');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        watch: {
            php: {
                files: ['**/*.php', '*.php']
            },
            html: {
                files: ['**/*.html', '*.html']
            }
            // css: {
			// 	files: '**/*.scss',
			// 	tasks: ['sass']
			// }
        },
        sass: {
			dist: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none'
                },
				files: {
					'public/style/css/style.css' : 'public/style/sass/style.scss'
				}
			}
		},
        browserSync: {
            dev: {
                bsFiles: {
                    src: '**/*.*'
                },
                options: {
                    proxy: 'online.dev:80', //our PHP server
                    port: 8080, // our new port
                    open: true,
                    watchTask: true
                }
            }
        },
        php: {
            dev: {
                options: {
                    port: 8010,
                    base: ''
                }
            }
        }
    });

    grunt.registerTask('default', ['php', 'browserSync', 'watch']);
};
