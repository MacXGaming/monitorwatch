<?php
namespace Core;

class Cache
{
    public static function get($key)
    {
        @include $_SERVER['DOCUMENT_ROOT']."/../Cache/$key";
        return isset($val) ? $val : false;
    }

    public static function set($key, $val)
    {
        $val = var_export($val, true);
        $val = str_replace('stdClass::__set_state', '(object)', $val);
        $tmp = $_SERVER['DOCUMENT_ROOT']."/../Cache/$key." . uniqid('', true) . '.tmp';
        file_put_contents($tmp, '<?php $val = ' . $val . ';', LOCK_EX);
        rename($tmp, $_SERVER['DOCUMENT_ROOT']."/../Cache/$key");
    }

    public static function exist($key)
    {
        // return false;
        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/../Cache/'.$key)) {
            if (time()-filemtime($_SERVER['DOCUMENT_ROOT'].'/../Cache/'.$key) > 300) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/../Cache/'.$key);
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static function delete($key)
    {
        return (file_exists($_SERVER['DOCUMENT_ROOT'].'/../Cache/'.$key)) ? unlink($_SERVER['DOCUMENT_ROOT'].'/../Cache/'.$key) : false;
    }

    public static function emptyResource($backendURL, $resourceId)
    {
        foreach (glob($_SERVER['DOCUMENT_ROOT']."/../Cache/{$backendURL}*_{$resourceId}_*") as $file) {
            unlink($file);
        }
    }
}
