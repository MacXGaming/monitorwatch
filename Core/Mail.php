<?php
namespace Core;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail{
    private const Host = "mail.bazzo.dk";
    private const SMTPAuth = true;
    private const Username = "no-reply@monitorwatch.net";
    private const Password = "9Fcf%86g";
    private const SMTPSecure = false;
    private const Port = 25;
    private const FromMail = "no-reply@monitorwatch.net";
    private const FromName = "MonitorWatch";

    public static function send($addresses = array(), $subject, $body){
        $mail = new PHPMailer(true);
        try {
            // $mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->Host = self::Host;
            $mail->SMTPAuth = self::SMTPAuth;
            $mail->SMTPAutoTLS = false;
            $mail->Username = self::Username;
            $mail->Password = self::Password;
            $mail->SMTPSecure = self::SMTPSecure;
            $mail->Port = self::Port;
            //Recipients
            $mail->setFrom(self::FromMail, self::FromName);
            foreach ($addresses as $address) {
                $mail->addAddress($address['address'], (isset($address['name']) ? $address['name'] : ""));
            }
            //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $body;

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }


}
