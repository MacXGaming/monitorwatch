<?php

namespace Core;

use \App\Config;

class Cookie {
    public static function set($name, $value, $uri = null, $time = 31546000) {
		if (!$uri) {
			$uri = '/'.Config::$SITE;
		}

		if (setcookie($name, $value, time()+$time,$uri,Config::URL,false,true)) {
			$_COOKIE[$name] = $value;
			return true;
		}
		return false;
	}

    public static function get($key){
        return (isset($_COOKIE[$key]) ? $_COOKIE[$key] : false);
    }

    public static function remove($key, $uri = null){
        if (!$uri) {
			$uri = '/'.Config::$SITE.'/';
		}
        setcookie($key, null, -1, $uri, Config::URL);
        unset($_COOKIE[$key]);
    }

}
