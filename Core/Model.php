<?php

namespace Core;

use Core\DB;
use App\Config;

/**
 * Base model
 *
 * PHP version 5.4
 */
abstract class Model {

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function getDB() {
        static $db = null;
        if ($db === null) {
            $db = new DB(Config::DB_HOST, Config::DB_NAME, Config::DB_USER, Config::DB_PASSWORD);
        }
        return $db;
    }
}
