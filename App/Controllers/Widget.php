<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Lang;
use App\Models\Users;
use App\Models\Monitor;
use App\Models\Log;
use \Core\Session;
use \Core\Alert;
use \Core\Cookie;
use \Core\Cache;
use \Core\Color;
use \Core\Mail;

/**
 * Pages controller
 *
 */
class Widget extends \Core\Controller
{
    private $userCheck = true;

    /**
     * Before filter
     *
     * @return void
     */
    protected function before()
    {
        $User = Users::GetByCode($_GET['Code']);
        if(!empty($User)){
            Session::set('userId', $User->id);
        }
        if (Session::get('userId')==null) {
            if ($this->route_params['action'] == "login" or $this->route_params['action'] == "register") {
            } else {
                header('location:/login');
                exit();
            }
        }
    }

    /**
     * After filter
     *
     * @return void
     */
    protected function after()
    {
        //echo " (after)";
    }

    public function normalAction()
    {
        $monitors = Monitor::getAll(Session::get('userId'));
        foreach ($monitors as $monitor) {
            $array[] = $monitor['info']->name;
        }
        View::renderTemplate('widgets/'.$this->route_params['id'].'.twig', array(
            'page'       => "index",
            'alert'      => Alert::print(),
            'user'       => Users::get(Session::get('userId')),
            'admin'      => (Session::get('adminId')==null ? 0 : Session::get('adminId')),
            'monitors'   => $monitors,
            'monitorArray'=>$array,
            'up'         => Monitor::getUp(Session::get('userId')),
            'down'       => Monitor::getDown(Session::get('userId')),
            'paused'     => Monitor::getPaused(Session::get('userId'))
        ));
    }
}
