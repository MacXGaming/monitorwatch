<?php

namespace App\Controllers;

use \Core\View;
use \Core\Alert;
use App\Models\Lang;
use App\Models\Users;
use \Core\Session;
use \Core\Cookie;
use \Core\Cache;
use \Core\Color;

/**
* Profile controller
*
*/
class Profile extends \Core\Controller
{
    private $userCheck = true;

    /**
    * Before filter
    *
    * @return void
    */
    protected function before()
    {
        if (Session::get('userId')==null) {
            if ($this->route_params['action'] == "login" or $this->route_params['action'] == "signup") {
            } else {
                header('location:/login');
                exit();
            }
        }
        $this->user = Users::get(Session::get('userId'));
    }

    /**
    * After filter
    *
    * @return void
    */
    protected function after()
    {
        //echo " (after)";
    }

    public function editAction()
    {

        if(isset($_POST['info'])){
            $name = $_POST['name'];
            $email = $_POST['email'];

            if(empty($name)){
                Alert::add(_('Please, tell us your name!'));
            }

            if(empty($email)){
                Alert::add(_('Please, tell us your email, otherwise you cannot login!'));
            }

            if(!Users::validateEmail($email)){
                Alert::add(_("Your email isn\'t valid!"));
            }

            if(empty(Alert::get())){
                Users::updateInfo(Session::get('userId'), $name, $email);

                Alert::add(_("Your profile is now updated!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            }else{
                Alert::set('danger');
            }
        }

        if(isset($_POST['password'])){
            $old = $_POST['old'];
            $new = $_POST['new'];
            $confirm = $_POST['confirm'];

            $user = $this->user;

            if(empty($old)){
                Alert::add(_('Enter your old password'));
            }

            if(!Users::verifyPassword($old, $user->password)){
                Alert::add(_("Your old password isn\'t the right one!"));
            }

            if(empty($new)){
                Alert::add(_("You have to enter in a new password!"));
            }

            if(strlen($new)<=5){
                Alert::add(_("Your password has to be 6 or more characters"));
            }

            if($new!=$confirm){
                Alert::add(_("Your new password doesn\'t match!"));
            }

            if(empty(Alert::get())){
                Users::updatePassword(Session::get('userId'), $new);

                Alert::add(_("Your profile password is now updated!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            }else{
                Alert::set('danger');
            }
        }

        View::renderTemplate('profile/edit.twig', array(
            'page'       => "profile.edit",
            'alert'      => Alert::print(),
            'user'       => $this->user,
            'admin'      => (Session::get('adminId')==null ? 0 : Session::get('adminId'))
        ));
    }
}
