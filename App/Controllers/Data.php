<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Lang;
use App\Models\Users;
use App\Models\Monitor;
use App\Models\Log;
use \Core\Session;
use \Core\Alert;
use \Core\Cookie;
use \Core\Cache;
use \Core\Color;

/**
 * Data controller
 *
 */
class Data extends \Core\Controller
{

    /**
     * Before filter
     *
     * @return void
     */
    protected function before()
    {
        if (isset($_GET['Code'])) {
            $User = Users::GetByCode($_GET['Code']);
            if (!empty($User)) {
                Session::set('userId', $User->id);
            }
        }
        if (Session::get('userId') == null) {
            if ($this->route_params['action'] == "login" or $this->route_params['action'] == "signup") {
            } else {
                header('location:/login');
                exit();
            }
        }
    }

    /**
     * After filter
     *
     * @return void
     */
    protected function after()
    {
        //echo " (after)";
    }

    public function quickdataAction()
    {
        header("Content-type: application/json");
        $data = array();
        $data['total'] = count(Monitor::getAll(Session::get('userId')));
        $data['up'] = Monitor::getUp(Session::get('userId'));
        $data['down'] = Monitor::getDown(Session::get('userId'));
        $data['paused'] = Monitor::getPaused(Session::get('userId'));
        $print = json_encode($data, JSON_NUMERIC_CHECK);
        echo str_replace('', '', $print);
    }

    public function livedataAction()
    {
        header("Content-type: application/json");

        $monitors = Monitor::getAll(Session::get('userId'));
        // $year=date("Y");
        // $month=date("m");
        // $day=date("d");
        // $hour=date("H");
        // $minute=date("i");
        foreach ($monitors as $monitor) {
            $latest = Log::getLatest($monitor['info']->id);
            if (empty($latest)) {
                // $array[] = array($year, $month, $day, $hour, $minute, 0, 0);
                continue;
            }
            $year = date("Y", strtotime($latest->date));
            $month = date("m", strtotime($latest->date));
            $day = date("d", strtotime($latest->date));
            $hour = date("H", strtotime($latest->date));
            $minute = date("i", strtotime($latest->date));
            $array[] = array($year, $month, $day, $hour, $minute, 0, $latest->ms);
        }
        $print = json_encode($array, JSON_NUMERIC_CHECK);
        echo str_replace('', '', $print);
    }

    public function dataAction()
    {
        header("Content-type: application/json");

        $monitors = Monitor::getAll(Session::get('userId'));
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $hour = date("H");
        $minute = date("i");
        $array = array();
        foreach ($monitors as $monitor) {
            if (strtolower($monitor['info']->name) == strtolower($_GET['name'])) {
                $datas = Log::get($monitor['info']->id);
                if (empty($datas)) {
                    continue;
                }

                foreach ($datas as $data) {
                    $year = date("Y", strtotime($data->date));
                    $month = date("m", strtotime($data->date));
                    $day = date("d", strtotime($data->date));
                    $hour = date("H", strtotime($data->date));
                    $minute = date("i", strtotime($data->date));
                    $second = date("s", strtotime($data->date));
                    $date = "Date.UTC($year,$month,$day,$hour,$minute,$second,0)";

                    $array[] = array($date, $data->ms);
                }
            }
            // $year=date("Y", strtotime($latest->date));
            // $month=date("m", strtotime($latest->date));
            // $day=date("d", strtotime($latest->date));
            // $hour=date("H", strtotime($latest->date));
            // $minute=date("i", strtotime($latest->date));

        }
        $print = json_encode($array, JSON_NUMERIC_CHECK);
        echo $_GET['callback'];
        echo "(";
        echo str_replace('"', '', $print) . "";
        echo ");";
    }

    public function testAction()
    {
    }

    public function customInsertAction()
    {
        if (isset($_POST)) {
            foreach ($_POST as $MonitorName => $Status) {
                $Monitor = Monitor::getByName(Session::Get('userId'), $MonitorName);
                if (!empty($Monitor)) {
                    Log::add($Monitor['info']->id, date("Y-m-d H:i:00"), $Status);
                }
            }
        }
    }
}
