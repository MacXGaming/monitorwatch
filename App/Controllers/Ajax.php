<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Lang;
use App\Models\Tasks;
use App\Models\Links;
use \Core\Session;
use \Core\Cookie;

/**
* Ajax controller
*
*/
class Ajax extends \Core\Controller
{
    private $projectId;
    /**
    * Before filter
    *
    * @return void
    */
    protected function before()
    {
        $this->projectId = 1;
        header('Content-Type: application/json');
    }

    /**
    * After filter
    *
    * @return void
    */
    protected function after()
    {
        //echo " (after)";
    }


    public function getTasksAction()
    {
        echo json_encode($this->tasks(Tasks::get($this->projectId)));
    }

    public function createTaskAction(){
        echo json_encode(Tasks::create($this->projectId, $_POST['name'], $_POST['start'], $_POST['end']));
    }

    public function deleteTaskAction(){
        echo json_encode(Tasks::delete($this->projectId, $_POST['taskId']));
    }

    public function moveTaskAction(){
        echo json_encode(Tasks::move($this->projectId, $_POST['taskId'], $_POST['start'], $_POST['end']));
    }

    public function getLinksAction(){
        echo json_encode(Links::get($this->projectId));
    }

    public function createLinkAction()
    {
        echo json_encode(Links::create($this->projectId, $_POST['from'], $_POST['to'], $_POST['type']));
    }
    public function deleteLinkAction()
    {
        echo json_encode(Links::delete($this->projectId, $_POST['linkId']));
    }

    public function moveRowAction(){
        echo json_encode(Tasks::moveRow($this->projectId, $_POST['source'], $_POST['target'], $_POST['position']));
    }

    public function setTypeAction()
    {
        return Session::set('type', $_POST['typeId']);
    }




    private function tasks($items) {
        $result = array();

        foreach($items as $item) {
          $r = new \stdClass();

          // rows
          $r->id = $item->id;
          $r->text = htmlspecialchars($item->name);
          $r->start = $item->start;
          $r->end = $item->end;
          $r->complete = $item->complete;
        //   $r->collab = '<div class="collabImage" style="float:left;"><img style="width: 20px;border-radius: 100%;" data-toggle="tooltip" data-placement="top" title="" data-title="René Dyhr (ansvarlig)" src="http://crm.gohosting.camp/upload/9.png"></div>';
        $r->collab = '';
          if ($item->milestone) {
              $r->type = 'Milestone';
          }

          $parent = $r->id;

          $children = Tasks::get($this->projectId, $parent);

          if (!empty($children)) {
              $r->children = $this->tasks($children);
          }

          $result[] = $r;
        }
        return $result;
    }

    public function testAction(){
        // echo \App\Models\Login::hashPassword('test');
        // print_r(Tasks::maxOrdinal());
    }
}
