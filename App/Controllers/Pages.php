<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Lang;
use App\Models\Users;
use App\Models\Monitor;
use App\Models\Log;
use \Core\Session;
use \Core\Alert;
use \Core\Cookie;
use \Core\Cache;
use \Core\Color;
use \Core\Mail;

/**
* Pages controller
*
*/
class Pages extends \Core\Controller
{
    private $userCheck = true;

    /**
    * Before filter
    *
    * @return void
    */
    protected function before()
    {
        if (Session::get('userId')==null) {
            if ($this->route_params['action'] == "login" or $this->route_params['action'] == "register") {
            } else {
                header('location:/login');
                exit();
            }
        }
    }

    /**
    * After filter
    *
    * @return void
    */
    protected function after()
    {
        //echo " (after)";
    }


    public function indexAction()
    {
        if (isset($_POST['addMonitor'])) {
            $type = intval($_POST['type']);
            $name = $_POST['name'];
            $target = $_POST['target'];
            $port = ($type == 2 ? $_POST['port'] : 0);
            $intervalCheck = (int) $_POST['interval'] + 0;

            if (empty($name)) {
                Alert::add(_("You have to give the monitor a name!"));
            }

            if (empty($target)) {
                $target = "Custom";
//                Alert::add(_("What\'s the target?"));
            }


            if (empty(Alert::get())) {
                Monitor::add(Session::get('userId'), $type, $name, $target, date("Y-m-d H:i:00"), $port, $intervalCheck);
                Alert::add(_("Monitor is now added!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            } else {
                Alert::set('danger');
            }
        }

        if(isset($_POST['edit'])){
            $monitorId = $_POST['monitorId'];
            if (!Monitor::check(Session::get('userId'), $monitorId)) {
                Alert::add(_("This monitor isn\'t yours!"));
            }
            $monitor = Monitor::get(Session::get('userId'), $monitorId);
            $type = $monitor['info']->type;
            $name = $_POST['name'];
            $target = $_POST['target'];
            $port = ($type == 2 ? $_POST['port'] : 0);
            $intervalCheck = (int) $_POST['interval'] + 0;


            if (empty($name)) {
                Alert::add(_("You have to give the monitor a name!"));
            }

            if (empty($target)) {
                $target = "Custom";
//                Alert::add(_("What\'s the target?"));
            }


            if (empty(Alert::get())) {
                Monitor::edit(Session::get('userId'), $monitorId, $name, $target, $port, $intervalCheck);
                Alert::add(_("Monitor is now edited!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            } else {
                Alert::set('danger');
            }
        }

        if (isset($_GET['pause'])) {
            $monitorId = $_GET['pause'];
            if (!Monitor::check(Session::get('userId'), $monitorId)) {
                Alert::add(_("This monitor isn\'t yours!"));
            }

            if (empty(Alert::get())) {
                Monitor::pause($monitorId);
                Alert::add(_("Monitor status is changed!"));
                Alert::set('success');
                header("Location:/");
                exit();
            } else {
                Alert::set('danger');
            }
        }

        if (isset($_GET['delete'])) {
            $monitorId = $_GET['delete'];
            if (!Monitor::check(Session::get('userId'), $monitorId)) {
                Alert::add(_("This monitor isn\'t yours!"));
            }

            if (empty(Alert::get())) {
                Monitor::delete($monitorId);
                Alert::add(_("Monitor are now deleted!"));
                Alert::set('success');
                header("Location:/");
                exit();
            } else {
                Alert::set('danger');
            }
        }

        $monitors = Monitor::getAll(Session::get('userId'));
        foreach ($monitors as $monitor) {
            $array[] = $monitor['info']->name;
        }

        View::renderTemplate('index.twig', array(
            'page'       => "index",
            'alert'      => Alert::print(),
            'user'       => Users::get(Session::get('userId')),
            'admin'      => (Session::get('adminId')==null ? 0 : Session::get('adminId')),
            'monitors'   => $monitors,
            'monitorArray'=>$array,
            'up'         => Monitor::getUp(Session::get('userId')),
            'down'       => Monitor::getDown(Session::get('userId')),
            'paused'     => Monitor::getPaused(Session::get('userId'))
        ));
    }

    public function logoutAction()
    {
        session_destroy();
        header("location:/");
    }

    public function loginAction()
    {
        if (isset($_POST['login'])) {
            $check = Users::check($_POST['email'], $_POST['password']);
            if ($check==false) {
                $this->userCheck = false;
            } else {
                Session::set('userId', $check->id);
                header("location:/");
                $this->userCheck = true;
            }
        }
        View::renderTemplate('login.twig', array(
            'page'       => "login",
            'alert'      => \Core\Alert::print(),
            'userCheck'  => $this->userCheck
        ));
    }

    public function registerAction()
    {
        if (isset($_POST['signup'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $repeatPassword = $_POST['repeatPassword'];

            if ($_POST['userAgreement'] != 1) {
                $errors[] = "";
            }

            if (Users::validateEmail($email)==false) {
                $errors[] = "";
            }

            if (Users::emailExist($email)==true) {
                $errors[] = "";
            }

            if ($password!=$repeatPassword) {
                $errors[] = "";
            }

            if (empty($errors)) {
                Users::signup($name, $email, $password);
                header("location:/login");
                exit();
            }
        }
        View::renderTemplate('register.twig', array(
            'page'       => "signup"
        ));
    }

    public function testAction()
    {
        echo "<pre>";
        self::start();

//        $url = "http://bazzo.dk";
//        $orignal_parse = parse_url($url, PHP_URL_HOST);
//        $scheme = parse_url($url, PHP_URL_SCHEME); // HTTPS OR HTTP
//
//        $get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
//        $read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
//        $cert = stream_context_get_params($read);
//        $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
//        print_r(date("Y-m-d H:i:s", $certinfo['validFrom_time_t']));
//        echo "<br>";
//        print_r(date("Y-m-d H:i:s", $certinfo['validTo_time_t']));
//        echo "<br>";
//        print_r($certinfo);




        echo "<br>";

        echo 'Elapsed time: ' . self::elapsed() . ' seconds';
        echo "</prE>";
    }
    private static $startTimes = array();
    /**
    * Start the timer
    *
    * @param $timerName string The name of the timer
    * @return void
    */
    public static function start($timerName = 'default')
    {
        self::$startTimes[$timerName] = microtime(true);
    }
    /**
    * Get the elapsed time in seconds
    *
    * @param $timerName string The name of the timer to start
    * @return float The elapsed time since start() was called
    */
    public static function elapsed($timerName = 'default')
    {
        return microtime(true) - self::$startTimes[$timerName];
    }
}
