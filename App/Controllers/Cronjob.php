<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Lang;
use App\Models\Users;
use App\Models\Monitor;
use App\Models\Log;
use \Core\Session;
use \Core\Alert;
use \Core\Cookie;
use \Core\Cache;
use \Core\Color;

/**
* Cronjob controller
*
*/
class Cronjob extends \Core\Controller
{

    /**
    * Before filter
    *
    * @return void
    */
    protected function before()
    {
        if ($_GET['ID']!="aabbcc") {
            exit();
        }
    }

    /**
    * After filter
    *
    * @return void
    */
    protected function after()
    {
        //echo " (after)";
    }


    public function updateAction()
    {
        $date = date("Y-m-d H:i:00");
        $monitors = Monitor::getAll(0, array(1));

        foreach ($monitors as $monitor) {
            $lastCheck = strtotime($monitor['info']->last_check);
            $currentDate = strtotime($date);
            if ($currentDate-$lastCheck>=$monitor['info']->interval_check) {
                if ($monitor['info']->type==1) {
                    $ms = $this->ping($monitor['info']->target, 255, 2);
                    Log::add($monitor['info']->id, $date, $ms);
                    Monitor::updateCheck($monitor['info']->id, $date);
                }
                if ($monitor['info']->type==2) {
                    $ms = $this->port($monitor['info']->target, $monitor['info']->port, 2);
                    Log::add($monitor['info']->id, $date, $ms);
                    Monitor::updateCheck($monitor['info']->id, $date);
                }
            }
        }
    }

    public function testAction()
    {
    }

    private function port($host, $port, $timeout)
    {
        $tB = microtime(true);
        try {
            $fP = fSockOpen($host, $port, $errno, $errstr, $timeout);
        } catch (\ErrorException $e) {
            return -1;
        }
        if (!$fP) { return "down"; }
        $tA = microtime(true);
        return round((($tA - $tB) * 1000), 0);
    }

    private function ping($host, $ttl, $timeout)
    {
        $ttl = escapeshellcmd($ttl);
        $timeout = escapeshellcmd($timeout);
        $host = escapeshellcmd($host);
        // Exec string for Windows-based systems.
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // -n = number of pings; -i = ttl; -w = timeout (in milliseconds).
            $exec_string = 'ping -n 1 -i ' . $ttl . ' -w ' . ($timeout * 1000) . ' ' . $host;
        }
        // Exec string for Darwin based systems (OS X).
        elseif (strtoupper(PHP_OS) === 'DARWIN') {
            // -n = numeric output; -c = number of pings; -m = ttl; -t = timeout.
            $exec_string = 'ping -n -c 1 -m ' . $ttl . ' -t ' . $timeout . ' ' . $host;
        }
        // Exec string for other UNIX-based systems (Linux).
        else {
            // -n = numeric output; -c = number of pings; -t = ttl; -W = timeout
            $exec_string = 'ping -n -c 1 -t ' . $ttl . ' -W ' . $timeout . ' ' . $host . ' 2>&1';
        }
        exec($exec_string, $output, $return);
        // If the result line in the output is not empty, parse it.
        $latency = -1;
        if (!empty($output[1])) {
            // Search for a 'time' value in the result line.
            $response = preg_match("/time(?:=|<)(?<time>[\.0-9]+)(?:|\s)ms/", $output[1], $matches);
            // If there's a result and it's greater than 0, return the latency.
            if ($response > 0 && isset($matches['time'])) {
                $latency = round($matches['time']);
            }
        }
        return $latency;
    }
}
