<?php

namespace App\Controllers\Admin;

use \Core\View;
use \Core\Alert;
use App\Models\Lang;
use App\Models\Domain;
use App\Models\Users;
use \Core\Session;
use \Core\Cookie;
use \Core\Cache;
use \Core\Color;

/**
* Customers admin controller
*
*/
class Customers extends \Core\Controller
{
    private $userCheck = true;

    /**
    * Before filter
    *
    * @return void
    */
    protected function before()
    {
        if (Session::get('userId')==null) {
            if ($this->route_params['action'] == "login" or $this->route_params['action'] == "signup") {
            } else {
                header('location:/login');
                exit();
            }
        }
        if(Session::get('adminId')==null){
            $userId = Session::get('userId');
        }else{
            $userId = Session::get('adminId');
        }

        $this->user = Users::get($userId);
        if($this->user->type!=2){
            header("Location:/");
            exit();
        }
    }

    public function backAction()
    {
        $userId = Session::get('adminId');
        $user = Users::get($userId);
        if($user==false){
            Alert::add(_("Customer doesn\'t exist!"));
            Alert::set('danger');
            header("Location:/");
            exit();
        }
        Session::set('userId', $userId);
        Session::remove('adminId');

        Alert::add(_("You are now back as: ".$user->name));
        Alert::set('success');
        header("Location:/");
        exit();
    }

    public function listAction()
    {

        if(isset($_GET['sign-in'])){
            $userId = $_GET['sign-in'];
            $user = Users::get($userId);
            if($user==false){
                Alert::add(_("Customer doesn\'t exist!"));
                Alert::set('danger');
                header("Location:/");
                exit();
            }
            Session::set('adminId', Session::get('userId'));
            Session::set('userId', $userId);

            Alert::add(_("You are now logged in as: ".$user->name));
            Alert::set('success');
            header("Location:/");
            exit();
        }

        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $type = $_POST['type'];
            $password = $_POST['password'];
            $confirm = $_POST['confirm'];

            if(empty($name)){
                Alert::add(_('Please, tell us your name!'));
            }

            if(empty($email)){
                Alert::add(_('Please, tell us your email, otherwise you cannot login!'));
            }

            if(!Users::validateEmail($email)){
                Alert::add(_("Your email isn\'t valid!"));
            }

            if(empty($password)){
                Alert::add(_("You have to enter in a new password!"));
            }

            if(strlen($password)<=5){
                Alert::add(_("Your password has to be 6 or more characters"));
            }

            if($password!=$confirm){
                Alert::add(_("Your new password doesn\'t match!"));
            }

            if(empty(Alert::get())){
                Users::signup($name, $email, $password, $type);

                Alert::add(_("The customer is now created!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            }else{
                Alert::set('danger');
            }
        }

        if(isset($_GET['delete'])){
            $userId = $_GET['delete'];
            $user = Users::get($userId);
            if($user==false){
                Alert::add(_("Customer doesn\'t exist!"));
                Alert::set('danger');
                header("Location:/");
                exit();
            }
            Users::delete($userId);

            Alert::add(_("Customer is now deleted!"));
            Alert::set('success');
            header("Location:/admin/customers/list");
            exit();
        }

        View::renderTemplate('customers/list.twig', array(
            'page'       => "customers.list",
            'alert'      => Alert::print(),
            'user'       => $this->user,
            'customers'  => Users::getAll()
        ));
    }

    public function editAction()
    {
        $userId = intval($this->route_params['id']);
        $customer = Users::get($userId);

        if($customer==false){
            header("Location:/");
            exit();
        }
        if(isset($_POST['info'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $type = $_POST['type'];

            if(empty($name)){
                Alert::add(_('Please, tell us your name!'));
            }

            if(empty($email)){
                Alert::add(_('Please, tell us your email, otherwise you cannot login!'));
            }

            if(!Users::validateEmail($email)){
                Alert::add(_("Your email isn\'t valid!"));
            }

            if(empty(Alert::get())){
                Users::updateInfo($userId, $name, $email);
                Users::updateType($userId, $type);

                Alert::add(_("Your profile is now updated!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            }else{
                Alert::set('danger');
            }
        }

        if(isset($_POST['password'])){
            $old = $_POST['old'];
            $new = $_POST['new'];
            $confirm = $_POST['confirm'];

            $user = $this->user;

            if(empty($old)){
                Alert::add(_('Enter your old password'));
            }

            if(!Users::verifyPassword($old, $user->password)){
                Alert::add(_("Your old password isn\'t the right one!"));
            }

            if(empty($new)){
                Alert::add(_("You have to enter in a new password!"));
            }

            if(strlen($new)<=5){
                Alert::add(_("Your password has to be 6 or more characters"));
            }

            if($new!=$confirm){
                Alert::add(_("Your new password doesn\'t match!"));
            }

            if(empty(Alert::get())){
                Users::updatePassword($userId, $new);

                Alert::add(_("Your profile password is now updated!"));
                Alert::set('success');
                header("Refresh:0;");
                exit();
            }else{
                Alert::set('danger');
            }
        }

        View::renderTemplate('customers/edit.twig', array(
            'page'       => "customers.list",
            'alert'      => Alert::print(),
            'user'       => $this->user,
            'customer'   => $customer,
        ));
    }
}
