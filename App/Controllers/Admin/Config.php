<?php

namespace App\Controllers\Admin;

use \Core\View;
use App\Models\ResourceTypes;
use App\Models\Resources;
use App\Models\Types;
use App\Models\Login;
use App\Models\Camps;
use App\Models\Lang;
use \Core\Session;

/**
* Config admin controller
*
*/
class Config extends \Core\Controller
{
    private $userCheck = true;

    /**
    * Before filter
    *
    * @return void
    */
    protected function before()
    {
        if (isset($_POST['login'])) {
            $check = Login::check($_POST['username'], $_POST['password']);
            if ($check==false) {
                $this->userCheck = false;
            } else {
                Session::set('admin_status', $check->status);
                Session::set('admin_userId', $check->id);
                Session::set('admin_siteId', $check->siteId);
                header("location:/admin/config/information");
                $this->userCheck = true;
            }
        }

        if (Session::get('admin_userId')==false && $this->route_params['action'] != "login") {
            header("location:/admin/config/login");
            return false;
        }
        if (Session::get('admin_userId')!=false && $this->route_params['action'] == "login") {
            header("location:/admin/config/information");
        }

        $this->campInfo = Camps::checkCampById(Session::get('admin_siteId'));
    }

    public function indexAction()
    {
        header("location:/admin/config/information");
    }

    public function informationAction()
    {
        if (isset($_POST['saveInformation'])) {
            Camps::saveInformation(Session::get('admin_siteId'), $_POST);
            header("refresh:0;");
            exit();
        }
        View::renderTemplate('admin/information.twig', array(
            'campInfo'  => $this->campInfo,
            'campText'  => Camps::getAllLang(Session::get('admin_siteId')),
            'languages' => Lang::getAll(),
            'GOOGLE_MAP'=> \App\Config::GOOGLE_MAP
        ));
    }

    public function resourcesAction()
    {
        if (isset($this->route_params['id'])) {
            $resource = Resources::getById(Session::get('admin_siteId'), $this->campInfo->urlBackend, \App\Config::$LANG, $this->route_params['id']);
            if (isset($_POST['saveResource'])) {
                Resources::updateUnit(true, $this->campInfo->id, $this->route_params['id'], $resource['resourceType']['info']->id, $_POST['longitude'], $_POST['latitude'], $_POST['status']);
                header("location:/admin/config/resources");
                exit();
            }

            View::renderTemplate('admin/resourcesEdit.twig', array(
                'campInfo'  => $this->campInfo,
                'campText'  => Camps::getAllLang(Session::get('admin_siteId')),
                'resource'      => $resource,
                'GOOGLE_MAP'=> \App\Config::GOOGLE_MAP
            ));
        } else {
            View::renderTemplate('admin/resources.twig', array(
                'campInfo'  => $this->campInfo,
                'campText'  => Camps::getAllLang(Session::get('admin_siteId')),
                'resources' => Resources::getAll(Session::get('admin_siteId'), $this->campInfo->urlBackend, \App\Config::$LANG)
            ));
        }
    }

    public function resourceTypesAction()
    {
        if (isset($this->route_params['id'])) {
            $resource = ResourceTypes::getAllById(Session::get('admin_siteId'), $this->route_params['id'], Lang::getLangByIso(\App\Config::$LANG)->id);
            if (isset($_POST['saveResourceType'])) {
                ResourceTypes::updateType(true, Session::get('admin_siteId'), $resource['info']->referenceId, $_POST['capacity'], 0, $_POST['status'], $_POST['typeId'], $_POST['defaultAdults']);
                foreach ($_POST['resource'] as $langId => $information) {
                    ResourceTypes::updateTypeData(Session::get('admin_siteId'), $resource['info']->referenceId, $langId, $information['name'], $information['shortDescription'], $information['longDescription']);
                }
                header("refresh:0;");
                exit();
            }
            View::renderTemplate('admin/resourceTypesEdit.twig', array(
                'campInfo'  => $this->campInfo,
                'resourceType'  => $resource,
                'languages' => Lang::getAll(),
                'types'     => Types::getAll(Session::get('admin_siteId'), Lang::getLangByIso(\App\Config::$LANG)->id)
            ));
        } elseif (isset($this->route_params['rid'])) {
            $resource = ResourceTypes::getAllById(Session::get('admin_siteId'), $this->route_params['rid'], Lang::getLangByIso(\App\Config::$LANG)->id);

            if (isset($_POST)) {
                if (!isset($_POST['imageBase64']) || !$data = $_POST['imageBase64']) {
                    $errors[] = 'No valid image uploaded';
                }

                if (empty($errors)) {

                    //Get the file information
                    list($file_type, $data) = explode(';', $data);
                    list($base, $data) = explode(',', $data);
                    $file_data = base64_decode($data);
                    $file_size = strlen($data);

                    if ($file_type != 'data:image/png') {
                        $errors[] = 'Could not read file format';
                    }

                    if ($file_size > 1048576 * 20) {
                        $errors[] = "ONLY images under 20MB are accepted for upload";
                    }

                    if (!$file_data) {
                        $errors[] = "Image data is corrupted";
                    }


                    if (empty($errors)) {
                        $ifp = fopen($_SERVER['DOCUMENT_ROOT']."/../tmp/".$this->campInfo->urlName.".png", "wb");
                        fwrite($ifp, $file_data);
                        fclose($ifp);

                        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/img/resources/' . $this->campInfo->urlName)) {
                            mkdir($_SERVER['DOCUMENT_ROOT'] . '/img/resources/' . $this->campInfo->urlName, 0755, true);
                        }
                        $path_base = $_SERVER['DOCUMENT_ROOT'] . '/img/resources/' . $this->campInfo->urlName.'/';

                        $data = array(
                        'name'=>$this->campInfo->urlName.".png",
                        'tmp_name'=>$_SERVER['DOCUMENT_ROOT']."/../tmp/".$this->campInfo->urlName.".png",
                        'size'=>$file_size
                    );

                        $Upload = new \Core\Upload($data);
                        $Upload->setMaxSize(20, "MB");
                        $Upload->AllowedTypes(array("image/png"));
                        $time = time();

                        $Upload->setPath($path_base);

                        if ($Upload->getImageHeight()>=667 && $Upload->getImageWidth()>=1000) {
                            $Upload->setName($this->route_params['rid']);
                            $Upload->setPrefix("medium_");
                            $Upload->setSuffix("_".$time);
                            $Upload->setWidth(1000);
                            $Upload->setHeight(667);
                            $Upload->Render();
                        }

                        $Upload->setName($this->route_params['rid']);
                        $Upload->setPrefix("small_");
                        $Upload->setSuffix("_".$time);
                        $Upload->setWidth(500);
                        $Upload->setHeight(333);
                        $Upload->Render();

                        $Upload->setName($this->route_params['rid']);
                        $Upload->setPrefix("thumb_");
                        $Upload->setSuffix("_".$time);
                        $Upload->setWidth(200);
                        $Upload->setHeight(133);
                        $Upload->Render();

                        $Upload->setName($this->route_params['rid']);
                        $Upload->setPrefix("cart_");
                        $Upload->setSuffix("_".$time);
                        $Upload->setWidth(80);
                        $Upload->setHeight(80);
                        $Upload->Render();

                        $Upload->Clean();
                        unlink($_SERVER['DOCUMENT_ROOT']."/../tmp/".$this->campInfo->urlName.".png");

                        ResourceTypes::addImage(Session::get('admin_siteId'), $this->route_params['rid'], $time);
                        header("Refresh:0;");
                    }
                }
            }
            View::renderTemplate('admin/resourceTypesImages.twig', array(
                'campInfo'  => $this->campInfo,
                'resourceType'  => $resource,
                'languages' => Lang::getAll(),
                'types'     => Types::getAll(Session::get('admin_siteId'), Lang::getLangByIso(\App\Config::$LANG)->id)
            ));
        } else {
            $resources = ResourceTypes::getAll(true, Session::get('admin_siteId'), Lang::getLangByIso(\App\Config::$LANG)->id);
            View::renderTemplate('admin/resourceTypes.twig', array(
                'campInfo'  => $this->campInfo,
                'resourceTypes'  => $resources
            ));
        }
    }

    public function loginAction()
    {
        View::renderTemplate('admin/login.twig', array(
            'userCheck'  => $this->userCheck
        ));
    }

    public function logoutAction()
    {
        Session::remove('admin_userId');
        Session::remove('admin_siteId');
        Session::remove('admin_status');
        header("location:/admin/config/login");
    }
}
