<?php

namespace App\Models;

use Core\DB;
use App\Config;
use Core\Mail;

/**
* Log model
*
*/
class Log extends \Core\Model
{
    public static function add(int $monitorId, $date, int $ms)
    {
        try {
            $db = static::getDB();
            if($ms==-1){
                $ms=0;
                $status=0;
            }else{
                $status=1;
            }

            $latest = $db->query('SELECT * FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? ORDER BY date DESC LIMIT 2', array($monitorId))->results();
            if($db->count()>1){
                if($latest[0]->status!=$latest[1]->status){
                    $monitor = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE id = ?', array($monitorId))->results()[0];
                    if($latest[0]->status){
                        // ONLINE
                        Mail::send(array(array("address"=>"bazzo39@gmail.com", "name"=>"René Dyhr"), array("address"=>"hh@goh.dk", "name"=>"Henrik Hansen")), 'Monitor ('.$monitor->name.') went online again', 'Your monitor ('.$monitor->name.') are now online again!<br>Date: '.date("Y-m-d H:i:s"));
                    }else{
                        // OFFLINE
                        Mail::send(array(array("address"=>"bazzo39@gmail.com", "name"=>"René Dyhr"), array("address"=>"hh@goh.dk", "name"=>"Henrik Hansen")), 'Monitor ('.$monitor->name.') went offline', 'Your monitor ('.$monitor->name.') are now offline!<br>Date: '.date("Y-m-d H:i:s"));
                    }
                }
            }

            $db->query('INSERT INTO '.Config::DB_PREFIX.'log (monitorId, date, ms, status)VALUES(?, ?, ?, ?)', array($monitorId, $date, $ms, $status));
            if (!$db->error()) {
                return $db->lastId();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getUptime($monitorId)
    {
        try {
            $db = static::getDB();
//            $online = $db->query('SELECT count(monitorId) as Amount FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? AND status = 1 AND date >= now() - INTERVAL 1 MONTH', array($monitorId))->last()->Amount;
//            $total = $db->query('SELECT count(monitorId) as Total FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? AND date >= now() - INTERVAL 1 MONTH', array($monitorId))->last();
            $data = $db->query("SELECT ( SELECT COUNT(*) FROM log WHERE monitorId = ? AND status = 1 ) AS online, ( SELECT COUNT(*) FROM log WHERE monitorId = ? ) AS total FROM dual", array($monitorId, $monitorId))->last();
            if($data->total==0){
                return 100;
            }
            if (!$db->error()) {
                return round(($data->online/$data->total)*100, 2);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getLatest($monitorId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? ORDER BY date DESC LIMIT 1', array($monitorId))->results();
            if ($db->count()>0) {
                return $results[0];
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function get($monitorId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? AND date >= now() - INTERVAL 1 DAY ORDER BY date ASC', array($monitorId))->results();
            if ($db->count()>0) {
                return $results;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
