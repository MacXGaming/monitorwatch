<?php

namespace App\Models;

use Core\DB;
use App\Config;

/**
* Users model
*
*/
class Users extends \Core\Model
{
    public static function check($email, $password)
    {
        try {
            $db = static::getDB();

            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'users WHERE email = ?', array($email))->results();
            if ($db->count()>0) {
                $results = $results[0];

                if (self::verifyPassword($password, $results->password)) {
                    $db->query('UPDATE '.Config::DB_PREFIX.'users SET date_updated = ? WHERE email = ?', array(date('Y-m-d H:i:s'), $email));
                    return $results;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function GetByCode($Code)
    {
        try {
            $db = static::getDB();

            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'users WHERE UUID = ?', array($Code))->results();
            if ($db->count()>0) {
                $results = $results[0];

                return $results;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function hashPassword($string)
    {
        $string = password_hash($string, PASSWORD_DEFAULT);
        return $string;
    }
    public static function verifyPassword($string, $hash)
    {
        return password_verify($string, $hash);
    }

    public static function validateEmail($string)
    {
        if (filter_var($string, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    public static function emailExist($email)
    {
        try {
            $db = static::getDB();
            $db->query('SELECT * FROM '.Config::DB_PREFIX.'users WHERE email = ?', array($email));
            if ($db->count()>0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function signup($name, $email, $password, $type)
    {
        try {
            $db = static::getDB();

            $password = self::hashPassword($password);
            $db->query('INSERT INTO '.Config::DB_PREFIX.'users (name, email, password, type)VALUES(?, ?, ?, ?)', array($name, $email, $password, $type));
            if($db->error()){
                return false;
            }else{
                return $db->lastId();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function get(int $userId)
    {
        try {
            $db = static::getDB();
            $result = $db->query('SELECT * FROM '.Config::DB_PREFIX.'users WHERE id = ?', array($userId))->results();
            if ($db->count()>0) {
                return $result[0];
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function delete(int $userId)
    {
        try {
            $db = static::getDB();
            $db->query('DELETE FROM '.Config::DB_PREFIX.'users WHERE id = ?', array($userId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAll()
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'users ORDER BY name ASC')->results();
            $users = array();
            foreach ($results as $user) {
                $users[] = array("user"=>$user);
            }
            if ($db->count()>0) {
                return $users;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function updateInfo(int $userId, $name, $email)
    {
        try {
            $db = static::getDB();
            $db->query('UPDATE '.Config::DB_PREFIX.'users SET name = ?, email = ? WHERE id = ?', array($name, $email, $userId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function updateType(int $userId, int $type)
    {
        try {
            $db = static::getDB();
            $db->query('UPDATE '.Config::DB_PREFIX.'users SET type = ? WHERE id = ?', array($type, $userId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function updatePassword(int $userId, $password)
    {
        try {
            $db = static::getDB();
            $db->query('UPDATE '.Config::DB_PREFIX.'users SET password = ? WHERE id = ?', array(self::hashPassword($password), $userId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
