<?php

namespace App\Models;

use Core\DB;
use App\Config;

/**
* Monitor model
*
*/
class Monitor extends \Core\Model
{
    public static function add(int $userId, int $type, string $name, string $target, $lastCheck, int $port = 0, int $intervalCheck = 60)
    {
        try {
            $db = static::getDB();

            $db->query('INSERT INTO '.Config::DB_PREFIX.'monitors (userId, type, name, target, last_check, port, interval_check, status)VALUES(?, ?, ?, ?, ?, ?, ?, ?)', array($userId, $type, $name, $target, $lastCheck, $port, $intervalCheck, 1));
            if (!$db->error()) {
                return $db->lastId();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function edit(int $userId, int $monitorId, string $name, string $target, int $port = 0, int $intervalCheck = 60)
    {
        try {
            $db = static::getDB();

            $db->query('UPDATE '.Config::DB_PREFIX.'monitors SET name = ?, target = ?, port = ?, interval_check = ? WHERE id = ? AND userId = ?', array($name, $target, $port, $intervalCheck, $monitorId, $userId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAll(int $userId = 0, $status = array(0, 1))
    {
        try {
            $db = static::getDB();
            if($userId>0){
                $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND status IN ('.implode(',',$status).')', array($userId))->results();
            }else{
                $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE status IN ('.implode(',',$status).')')->results();
            }
            if ($db->count()>0) {
                $data = array();
                foreach ($results as $result) {
                    $data[] = array("info"=>$result, "uptime"=>Log::getUptime($result->id), "latest"=>Log::getLatest($result->id));
                }
                return $data;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function get(int $userId, int $monitorId, $status = array(0, 1))
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND id = ? AND status IN ('.implode(',',$status).')', array($userId, $monitorId))->results();
            if ($db->count()>0) {
                foreach ($results as $result) {
                    $data = array("info"=>$result, "uptime"=>Log::getUptime($result->id));
                }
                return $data;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getByName(int $userId, $monitorName, $status = array(0, 1))
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND name = ? AND status IN ('.implode(',',$status).')', array($userId, $monitorName))->results();
            if ($db->count()>0) {
                foreach ($results as $result) {
                    $data = array("info"=>$result, "uptime"=>Log::getUptime($result->id));
                }
                return $data;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function check(int $userId, int $monitorId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND id = ?', array($userId, $monitorId))->results();
            if ($db->count()>0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function updateCheck(int $monitorId, $date)
    {
        try {
            $db = static::getDB();

            $db->query('UPDATE '.Config::DB_PREFIX.'monitors SET last_check = ? WHERE id = ?', array($date, $monitorId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function pause(int $monitorId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE id = ?', array($monitorId))->results()[0];
            $db->query('UPDATE '.Config::DB_PREFIX.'monitors SET status = ? WHERE id = ?', array(($results->status ? 0 : 1), $monitorId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function delete(int $monitorId)
    {
        try {
            $db = static::getDB();
            $db->query('DELETE FROM '.Config::DB_PREFIX.'monitors WHERE id = ?', array($monitorId));
            $db->query('DELETE FROM '.Config::DB_PREFIX.'log WHERE monitorId = ?', array($monitorId));
            if (!$db->error()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getUp(int $userId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND status = 1', array($userId))->results();
            if ($db->count()>0) {
                $data = array();
                foreach ($results as $monitor) {
                    $log = $db->query('SELECT * FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? ORDER BY date DESC LIMIT 1', array($monitor->id))->results();
                    if($db->count()==1) {
                        if($log[0]->status==1){
                            $data[] = $monitor->id;
                        }
                    }
                }
                return $data;
            } else {
                return array();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getDown(int $userId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND status = 1', array($userId))->results();
            if ($db->count()>0) {
                $data = array();
                foreach ($results as $monitor) {
                    $log = $db->query('SELECT * FROM '.Config::DB_PREFIX.'log WHERE monitorId = ? ORDER BY date DESC LIMIT 1', array($monitor->id))->results();
                    if($db->count()==1) {
                        if($log[0]->status==0){
                            $data[] = $monitor->id;
                        }
                    }
                }
                return $data;
            } else {
                return array();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getPaused(int $userId)
    {
        try {
            $db = static::getDB();
            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'monitors WHERE userId = ? AND status = 0', array($userId))->results();
            if ($db->count()>0) {
                $data = array();
                foreach ($results as $monitor) {
                    $data[] = $monitor->id;
                }
                return $data;
            } else {
                return array();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
