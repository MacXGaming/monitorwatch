<?php

namespace App\Models;

use Core\DB;
use \App\Config;

/**
 * Lang model
 *
 */
class Lang extends \Core\Model {

    public static function getLangByISO($iso) {
        try {
            $db = static::getDB();

            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'_languages WHERE iso = ?', array($iso))->results();

            if($db->count()>0){
                return $results[0];
            }else{
                return false;
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getLangById($id) {
        try {
            $db = static::getDB();

            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'_languages WHERE id = ?', array($id))->results();

            if($db->count()>0){
                return $results[0];
            }else{
                return false;
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAll() {
        try {
            $db = static::getDB();

            $results = $db->query('SELECT * FROM '.Config::DB_PREFIX.'_languages')->results();

            if($db->count()>0){
                return $results;
            }else{
                return false;
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
