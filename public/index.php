<?php
session_start();
date_default_timezone_set('Europe/Copenhagen');
/**
* Front controller
*
*/
/**
* Composer
*/

require '../vendor/autoload.php';
if (isset($_GET['lang'])) {
    \Core\Session::set('lang', $_GET['lang']);
}
if (\Core\Session::get('lang')!=false) {
    \App\Config::setLang(\Core\Session::get('lang'));
} else {
    \Core\Session::set('lang', \App\Config::$LANG);
}
$language = \Core\Session::get('lang').".utf8";
putenv("LANGUAGE=".$language);
putenv('LC_ALL='.$language);
setlocale(LC_ALL, $language);
$domain = 'messages';
bindtextdomain($domain, $_SERVER['DOCUMENT_ROOT']."/../locale");
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);

if ($_SERVER["HTTPS"] != "on" AND $_SERVER['SERVER_NAME']!="monitor.test") {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

function validateDate($date, $format = 'Y-m-d H:i:s')
{
    if (!$date) {
        return false;
    }
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

/**
* Twig
*/
Twig_Autoloader::register();

/**
* Error and Exception handling
*/
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
* Routing
*/
$router = new Core\Router();

// Add the routes
// $router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('', ['controller' => 'Pages', 'action' => 'index']);
// $router->add('login', ['controller' => 'Pages', 'action' => 'login']);
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);
$router->add('admin/{controller}/{action}/{id:\w+}', ['namespace' => 'Admin']);
$router->add('{controller}/{action}/{id:\w+}');
$router->add('{action}', ['controller' => 'Pages']);
$router->add('{controller}/{action}');


// $router->add('{camp:\w+}', ['controller' => 'Pages',   'action' => 'index']);
// $router->add('{camp:\w+}/', ['controller' => 'Pages',   'action' => 'index']);
// $router->add('{camp:\w+}/{id:\d+}', ['controller' => 'Pages',   'action' => 'index']);
// $router->add('{camp:\w+}/conditions', ['controller' => 'Pages',   'action' => 'conditions']);
// $router->add('{camp:\w+}/cart', ['controller' => 'Pages',   'action' => 'cart']);
// $router->add('{camp:\w+}/information', ['controller' => 'Pages',   'action' => 'information']);
// $router->add('{camp:\w+}/payment', ['controller' => 'Pages',   'action' => 'payment']);
// $router->add('{camp:\w+}/validation', ['controller' => 'Pages',   'action' => 'validation']);
// $router->add('{camp:\w+}/confirmation', ['controller' => 'Pages',   'action' => 'confirmation']);
// $router->add('{camp:\w+}/login', ['controller' => 'Pages',   'action' => 'login']);
// $router->add('{camp:\w+}/install', ['controller' => 'Install', 'action' => 'install']);
// $router->add('{camp:\w+}/install/resources', ['controller' => 'Install', 'action' => 'resources']);
// $router->add('{camp:\w+}/install/oldData', ['controller' => 'Install', 'action' => 'oldData']);
// $router->add('{camp:\w+}/install/test', ['controller' => 'Install', 'action' => 'test']);
// $router->add('{camp:\w+}/test', ['controller' => 'Pages',   'action' => 'test']);
// $router->add('{controller}/{action}');

// $router->add('{controller}/{action}');
// $router->add('{controller}/{id:\d+}/{action}');
// $router->add('admin/{controller}/{action}');

$router->dispatch($_SERVER['QUERY_STRING']);
