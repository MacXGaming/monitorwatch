$( document ).ready(function() {



    var UPDATE_INTERVAL = 5000;


    function requestLIVE() {

        $.ajax({
            url: '/data/livedata',
            success: function(point) {
                var chart = $('#livedata').highcharts();
                var series = chart.series[0];
                $.each(names, function (i, name) {
                    var time = Date.UTC(point[i][0], point[i][1], point[i][2], point[i][3], point[i][4], point[i][5], 0);
                    var last = localStorage.getItem("lastLIVE");
                    chart.series[i].addPoint([time, point[i][6]]);
                });
            },
            cache: false
        });

        $.ajax({
            url: '/data/quickdata',
            success: function(data) {
                var up = $('#up').highcharts();
                if (up) {
                    point = up.series[0].points[0];
                    point.update(data['up'].length);
                }
                var down = $('#down').highcharts();
                if (down) {
                    point = down.series[0].points[0];
                    point.update(data['down'].length);
                }
                var paused = $('#paused').highcharts();
                if (paused) {
                    point = paused.series[0].points[0];
                    point.update(data['paused'].length);
                }
            },
            cache: false
        });


    }
    // requestLIVE();

    var seriesOptions = [],
    seriesCounter = 0;
    // Create a timer
    var start = +new Date();
    function createChart() {

        $('#livedata').highcharts('StockChart',{
            chart : {
                events : {
                    load : function () {
                        setInterval(function () {
                            requestLIVE();
                        }, UPDATE_INTERVAL);
                        if (!window.isComparing) {
                            this.setTitle(null, {
                                text: 'Built chart in ' + (new Date() - start) + 'ms'
                            });
                        }
                    }
                },
                zoomType: 'x'
            },
            title: {
                text: 'Response Time'
            },
            subtitle: {
                text: 'Built chart in ...' // dummy text to reserve space for dynamic subtitle
            },
            legend : {
                enabled : true
            },
            navigator : {
                adaptToUpdatedData: true
            },
            scrollbar: {
                liveRedraw: false
            },
            rangeSelector: {
                buttons: [{
                    type: 'minute',
                    count: 5,
                    text: '5m'
                }, {
                    type: 'hour',
                    count: 1,
                    text: '1h'
                }, {
                    type: 'hour',
                    count: 2,
                    text: '2h'
                }, {
                    type: 'hour',
                    count: 12,
                    text: '12h'
                }, {
                    type: 'all',
                    text: 'All'
                }],
                selected: 0,
                inputEnabled: false
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            yAxis: {
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} ms.</b><br/>',
                valueDecimals: 1
            },

            series: seriesOptions,
            plotOptions: {
                series: {
                    connectNulls: true
                }
            }
        });
    }

    createChart();

    $.each(names, function (i, name) {
        $.getJSON('/data/data?name=' + name.toLowerCase() + '&callback=?',    function (data) {
            seriesOptions[i] = {
                name: name,
                data: data
            };

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
            }
        });
    });


    $('#up').highcharts({
        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: [{
                outerRadius: '100%',
                innerRadius: '80%',
                backgroundColor: "lightgrey",
                borderWidth: 0
            }]
        },

        tooltip: {
            enabled: false
        },
        yAxis: {
            min: 0,
            max: monitors,
            title: {
                text: null
            },

            stops: [
                [0.1, '#28a745'], // green
                [0.5, '#28a745'], // yellow
                [0.9, '#28a745'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                enabled: false
            }
        },

        credits: {
            enabled: false
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    enabled: true,
                    y: -38,
                    borderWidth: 0,
                    useHTML: true,
                    format: '<div style="text-align:center"><span style="font-size:45px;color:#000;">{y}</span></div>'
                }
            }
        },

        series: [{
            innerRadius: '80%',
            name: null,
            data: [up]
        }]

    });

    $('#down').highcharts({
        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: [{
                outerRadius: '100%',
                innerRadius: '80%',
                backgroundColor: "lightgrey",
                borderWidth: 0
            }]
        },


        tooltip: {
            enabled: false
        },
        yAxis: {
            min: 0,
            max: monitors,
            title: {
                text: null
            },

            stops: [
                [0.1, '#dc3545'], // green
                [0.5, '#dc3545'], // yellow
                [0.9, '#dc3545'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                enabled: false
            }
        },

        credits: {
            enabled: false
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    enabled: true,
                    y: -38,
                    borderWidth: 0,
                    useHTML: true,
                    format: '<div style="text-align:center"><span style="font-size:45px;color:#000;">{y}</span></div>'
                }
            }
        },

        series: [{
            innerRadius: '80%',
            name: null,
            data: [down]
        }]

    });

    $('#paused').highcharts({
        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: [{
                outerRadius: '100%',
                innerRadius: '80%',
                backgroundColor: "lightgrey",
                borderWidth: 0
            }]
        },


        tooltip: {
            enabled: false
        },
        yAxis: {
            min: 0,
            max: monitors,
            title: {
                text: null
            },

            stops: [
                [0.1, '#28a745'], // green
                [0.5, '#28a745'], // yellow
                [0.9, '#28a745'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                enabled: false
            }
        },

        credits: {
            enabled: false
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    enabled: true,
                    y: -38,
                    borderWidth: 0,
                    useHTML: true,
                    format: '<div style="text-align:center"><span style="font-size:45px;color:#000;">{y}</span></div>'
                }
            }
        },

        series: [{
            innerRadius: '80%',
            name: null,
            data: [paused]
        }]

    });


});
