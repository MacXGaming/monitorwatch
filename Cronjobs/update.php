<?php
date_default_timezone_set('Europe/Copenhagen');
include('/var/www/vhosts/monitorwatch.net/httpdocs/App/Config.php');
include('/var/www/vhosts/monitorwatch.net/httpdocs/Core/DB.php');
include('/var/www/vhosts/monitorwatch.net/httpdocs/vendor/autoload.php');
include('/var/www/vhosts/monitorwatch.net/httpdocs/Core/Mail.php');

$db = new \Core\DB(\App\Config::DB_HOST, \App\Config::DB_NAME, \App\Config::DB_USER, \App\Config::DB_PASSWORD);
function port($host, $port, $timeout)
{
    $tB = microtime(true);
    try {
        $fP = @fSockOpen($host, $port, $errno, $errstr, $timeout);
    } catch (Exception $e) {
        return -1;
    }
    if (!$fP) {
        return -1;
    }
    $tA = microtime(true);
    return round((($tA - $tB) * 1000), 0);
}

function getUptime($monitorId)
{
    global $db;
    try {
        $online = $db->query('SELECT * FROM '.\App\Config::DB_PREFIX.'log WHERE monitorId = ? AND status = 1', array($monitorId))->count();
        $total = $db->query('SELECT * FROM '.\App\Config::DB_PREFIX.'log WHERE monitorId = ?', array($monitorId))->count();
        if ($total==0) {
            return 100;
        }
        if (!$db->error()) {
            return round(($online/$total)*100, 2);
        } else {
            return false;
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


function getAll(int $userId = 0, $status = array(0, 1))
{
    global $db;
    try {
        if ($userId>0) {
            $results = $db->query('SELECT * FROM '.\App\Config::DB_PREFIX.'monitors WHERE userId = ? AND status IN ('.implode(',', $status).')', array($userId))->results();
        } else {
            $results = $db->query('SELECT * FROM '.\App\Config::DB_PREFIX.'monitors WHERE status IN ('.implode(',', $status).')')->results();
        }
        if ($db->count()>0) {
            $data = array();
            foreach ($results as $result) {
                $data[] = array("info"=>$result);
            }
            return $data;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function updateCheck(int $monitorId, $date)
{
    global $db;
    try {
        $db->query('UPDATE '.\App\Config::DB_PREFIX.'monitors SET last_check = ? WHERE id = ?', array($date, $monitorId));
        if (!$db->error()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


function ping($host, $ttl, $timeout)
{
    $ttl = escapeshellcmd($ttl);
    $timeout = escapeshellcmd($timeout);
    $host = escapeshellcmd($host);
    // Exec string for Windows-based systems.
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        // -n = number of pings; -i = ttl; -w = timeout (in milliseconds).
        $exec_string = 'ping -n 1 -i ' . $ttl . ' -w ' . ($timeout * 1000) . ' ' . $host;
    }
    // Exec string for Darwin based systems (OS X).
    elseif (strtoupper(PHP_OS) === 'DARWIN') {
        // -n = numeric output; -c = number of pings; -m = ttl; -t = timeout.
        $exec_string = 'ping -n -c 1 -m ' . $ttl . ' -t ' . $timeout . ' ' . $host;
    }
    // Exec string for other UNIX-based systems (Linux).
    else {
        // -n = numeric output; -c = number of pings; -t = ttl; -W = timeout
        $exec_string = 'ping -n -c 1 -t ' . $ttl . ' -W ' . $timeout . ' ' . $host . ' 2>&1';
    }
    exec($exec_string, $output, $return);
    // If the result line in the output is not empty, parse it.
    $latency = -1;
    if (!empty($output[1])) {
        // Search for a 'time' value in the result line.
        $response = preg_match("/time(?:=|<)(?<time>[\.0-9]+)(?:|\s)ms/", $output[1], $matches);
        // If there's a result and it's greater than 0, return the latency.
        if ($response > 0 && isset($matches['time'])) {
            $latency = round($matches['time']);
        }
    }
    return $latency;
}

function add(int $monitorId, $date, int $ms)
{
    global $db;
    try {
        if ($ms==-1) {
            $ms=0;
            $status=0;
        } else {
            $status=1;
        }

        $latest = $db->query('SELECT * FROM '.\App\Config::DB_PREFIX.'log WHERE monitorId = ? ORDER BY date DESC LIMIT 2', array($monitorId))->results();
        if ($db->count()>1) {
            if ($latest[0]->status!=$latest[1]->status) {
                $monitor = $db->query('SELECT * FROM '.\App\Config::DB_PREFIX.'monitors WHERE id = ?', array($monitorId))->results()[0];
                if ($latest[0]->status) {
                    // ONLINE
                    \Core\Mail::send(array(array("address"=>"bazzo39@gmail.com", "name"=>"René Dyhr")), 'Monitor ('.$monitor->name.') went online again', 'Your monitor ('.$monitor->name.') are now online again!<br>Date: '.date("Y-m-d H:i:s"));
                } else {
                    // OFFLINE
                    \Core\Mail::send(array(array("address"=>"bazzo39@gmail.com", "name"=>"René Dyhr")), 'Monitor ('.$monitor->name.') went offline', 'Your monitor ('.$monitor->name.') are now offline!<br>Date: '.date("Y-m-d H:i:s"));
                }
            }
        }

        $db->query('INSERT INTO '.\App\Config::DB_PREFIX.'log (monitorId, date, ms, status)VALUES(?, ?, ?, ?)', array($monitorId, $date, $ms, $status));
        if (!$db->error()) {
            return $db->lastId();
        } else {
            return false;
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


$date = date("Y-m-d H:i:00");
$monitors = getAll(0, array(1));
foreach ($monitors as $monitor) {
    $lastCheck = strtotime($monitor['info']->last_check);
    $currentDate = strtotime($date);
    if ($currentDate-$lastCheck>=$monitor['info']->interval_check) {
        if ($monitor['info']->type==1) {
            $ms = ping($monitor['info']->target, 255, 2);
            add($monitor['info']->id, $date, $ms);
            updateCheck($monitor['info']->id, $date);
        }
        if ($monitor['info']->type==2) {
            $ms = port($monitor['info']->target, $monitor['info']->port, 2);
            add($monitor['info']->id, $date, $ms);
            updateCheck($monitor['info']->id, $date);
        }
    }
}
